<?php
include "session_info.php";
$title = "Beta List Page " . ((isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1);
include "header.php"; ?>    
    <div class="container" style="padding-top:-5;">
      <div class = "jumbotron">
       <ul>
        <?php
$pending = (isset($_GET['pending']));
if ($pending && ($_SESSION['rank'] == 1 || !isset($_SESSION['rank']))) {
    header("Location: /projects");
    exit;
}
$type = isset($_GET['type']) ? $_GET['type'] : 3;
if (!is_numeric($type) || abs($type) > 3) {
    $type = 3;
} else $type = abs($type);
$page = isset($_GET['page']) ? $_GET['page'] : 1;
//if(!is_numeric($page)) $page = 1;
$con = getCon();
if (mysqli_connect_errno()) {
    echo "Failed to connect to the remote database :( Please let us know so we can fix this!";
    exit;
}
$query = mysqli_query($con, "SELECT BID, TITLE, DESCRIPTION FROM PENDING_BETAS ORDER BY BID LIMIT ".(($page-1)*10).", 10");
$numrows = mysqli_num_rows($query);
if ($numrows == 0) echo "There doesn't seem to be anything here at the moment. Please check back later.";
else while ($row = mysqli_fetch_array($query)) {
    if (strlen(trim($row['TITLE'])) == 0 || $i >= 10) {
        break;
    }
    echo '<li><h3>'; 
    echo '<a href="pending_beta?id='.$row['BID'].'">'. san($row['TITLE']) . "</a>:</h3>";
    echo "<h5>" . san(substr($row['DESCRIPTION'], 0, 250));
    if (strlen($row['DESCRIPTION'] > 250)) echo '...';
        echo "</h5>";
        echo "<h5><a href = approve?id=$row[BID]>Aprove</a> <a href = reject?id=$row[BID]>Reject</a></h5> ";
}
echo "</ul></div><p>Page ";
for ($i = 1;$i < $numrows / 10 + 1;$i = $i + 1) {
    if (intval($page) == $i) echo "<b>$i</b>";
    else if ($pending == false) echo "<a href = \"projects?type=" . $type . "&page=" . $i . "\"> " . $i . " </a>";
    else echo "<a href = \"pending_projects&page=$i\">$i</a>";
}
mysqli_close($con);
?>
    </div>
<?php include "footer.php"; ?>
