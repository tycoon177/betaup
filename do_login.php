<?php
session_start();
$user = trim($_POST[username]);
$pass = trim($_POST[password]);
if (empty($user) || empty($pass)) {
    header("Location: http://www.betaup.org/login");
    exit;
}
$con = mysqli_connect("localhost", "web", "qwerty23658", "BETAS");
if ($stmt = mysqli_prepare($con, "SELECT PASSWD_HASH, UID, RANK FROM USERS WHERE USERNAME=?")) {
    mysqli_stmt_bind_param($stmt, "s", $user);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $hash, $id, $rank);
    mysqli_stmt_fetch($stmt);
    if ($hash == enc($pass, $user)) {
        $_SESSION['UID'] = $id;
        $_SESSION['NAME'] = $user;
        $_SESSION['rank'] = $rank;
        header("Location: /");
        mysqli_close($con);
        exit;
    } else {
	echo "<b>".$hash." ".$id." ".$user." ".enc($pass, $user);
        header("Location: login?err");
        mysqli_close($con);
        exit;
    }
} else echo mysqli_errno($con);
print_r($_POST);
function enc($pwd, $user) {
    return hash('sha512', $pwd . hash('md5', $user));
}
?>
