<?PHP
include "session_info.php";
$title = "Submit a Beta";
if (!isset($_SESSION['time'])) $_SESSION['time'] = 0;
if (!isset($_SESSION['UID'])) {
    header("Location: /login?err");
    exit;
}
if (!empty($_POST['title']) && !empty($_POST['description'])) {
    $con = getCon();
    $game = isset($_POST['is_game']) ? ($_POST['is_game'] == 1 ? 1 : 0) : 0;
    if (!($stmt = mysqli_prepare($con, "INSERT INTO PENDING_BETAS VALUES(NULL,?,?,?,?)"))) echo mysqli_errno($con); //UID, TITLE, Description, IS_GAME
    mysqli_stmt_bind_param($stmt, "issi", $_SESSION['UID'], $_POST['title'], $_POST['description'], $game);
    mysqli_stmt_execute($stmt);

//    $con = getCon();
    $stmt = mysqli_prepare($con, "INSERT INTO PENDING_IMAGES VALUES (null,?,?)");
    if (!empty($_POST['img1'])) {
        mysqli_stmt_bind_param($stmt, "si", $_POST['img1'], $id);
        mysqli_stmt_execute($stmt);
    }
    if (!empty($_POST['img2'])) {
        mysqli_stmt_bind_param($stmt, "si", $_POST['img2'], $id);
        mysqli_stmt_execute($stmt);
    }
    if (!empty($_POST['img3'])) {
        mysqli_stmt_bind_param($stmt, "si", $_POST['img3'], $id);
        mysqli_stmt_execute($stmt);
    }
    if (!empty($_POST['img4'])) {
        mysqli_stmt_bind_param($stmt, "si", $_POST['img4'], $id);
        mysqli_stmt_execute($stmt);
    }
    if (!empty($_POST['img5'])) {
        mysqli_stmt_bind_param($stmt, "si", $_POST['img5'], $id);
        mysqli_stmt_execute($stmt);
    }
    if (!empty($_POST['img6'])) {
        mysqli_stmt_bind_param($stmt, "si", $_POST['img6'], $id);
        mysqli_stmt_execute($stmt);
    }
    if (!empty($_POST['img7'])) {
        mysqli_stmt_bind_param($stmt, "si", $_POST['img7'], $id);
        mysqli_stmt_execute($stmt);
    }
    if (!empty($_POST['img8'])) {
        mysqli_stmt_bind_param($stmt, "si", $_POST['img8'], $id);
        mysqli_stmt_execute($stmt);
    }
    /*  $con = getCon();
        $stmt=mysqli_query($con, "SELECT MAX(ID) FROM BETA WHERE UID = $_SESSION[UID]");
        $res = mysqli_fetch_array($stmt);
        $id = $res[0];*/
    header("Location: /submit?success");
    exit;
}
include "header.php";
?>
<script>
    var num = 4;
function addImg(){
if(num <= 8)
document.getElementById("images").innerHTML =  document.getElementById("images").innerHTML+"<input type=\"text\" name=\"img"+num+"\" placeholder=\"Link to Image "+num+"\" size=\"75\"><br><br>";
                num++;
        }
</script>

 <div class="container">
    <br><br>
    <?php
if (isset($_GET[success])) echo "<p style = 'color:green';>Your beta has been submitted for review! Check back later to see if it has been posted!</p></br></br>";
?>
    <h1><b>Submit A Beta</b></h1><br>
      <form action="/submit" method="post">
        <label for="title"><b>Title</b></label><br>
        <input type="text" name="title" placeholder="Title of Beta" size="75"><br><br>
        <label for="description"><b>Description</b></label><br>
        <textarea name="description" placeholder="This is where you will describe you beta"
        rows="5" cols="77"></textarea><br><br>
        <h4>Link to up to 8 photos to be displayed for your beta!</h4>
         <div id="images">
		   <input type="text" name="img1" placeholder="Link to Image 1" size="75"><br><br>
           <input type="text" name="img2" placeholder="Link to Image 2" size="75"><br><br>
           <input type="text" name="img3" placeholder="Link to Image 3" size= "75"><br><br>
         </div>
         <button class="btn-primary btn-md" onclick="addImg();return false">Add another image</button><br><br>
		 <input type="checkbox" value="1" name="is_game">Is this a game? (If it is a program instead, leave this unchecked)<br>
        <input type="submit" value="Submit Beta" class="btn btn-md btn-primary" ></input>
      </form>
    </div>
</BODY></HTML><?php include "footer.php"; ?>

