function getAjax(){
    var xhr;
    if (window.ActiveXObject)
    {
        try
        {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch(e)
        {
           // alert(e.message);
            xhr = null;
        }
    }
    else
    {
        xhr = new XMLHttpRequest();
    }

    return xhr;
}

function vote(v){
  var a = getAjax();
  a.open("GET","vote?bid=".concat(document.URL.substring(26)).concat("&vote=").concat(v), true);
  a.send();
  return a;
}
