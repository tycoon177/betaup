<?php
/*
 *
 * CONFIRMSIGNUP.PHP THURSDAY DECEMBER 19, 2013
 * BY: Dallas Nicholson <nicholson.dallasc@gmail.com>
 * BY: Ben Mchone <benjamin.mchone@gmail.com>
 *
 * This php script is to be ran after the signup form is submitted.
 * This php script checks to make sure the information submitted to the form is unique.
 * This php script does not check to make sure the information submitted is correct,
 * it simply makes sure the information will not harm the database nor allow XSS and make
 * sure the information is unique and has not been used before with another account.
 *
*/
include "session_info.php";
include_once ("mysql_lib.php");
include_once "dkim.php";
$con = mysqli_connect("localhost", "web", "qwerty23658", "BETAS");
/*
 * Check to see if the form was submitted or not. If not, redirect the user to the register page.
*/
if (!isset($_POST['password0']) || !isset($_POST['password']) || !isset($_POST['username']) || !isset($_POST['email'])) {
    header('Location: /register');
    exit;
    /*
     * The form was submitted, now check if any of the fields were empty.
     * Redirect to the register form with the error code if not.
    */
} else {
    print_r($_POST);
    $password0 = sani($_POST['password0']);
    $password = sani($_POST['password']);
    $username = sani($_POST['username']);
    $email = sani($_POST['email']);
    $bio = sani($_POST['interests']);
    echo "Here";
    if (empty($password0) || empty($password) || empty($username) || empty($email)) {
        header('Location: /register?err=0');
        exit;
        /*
         * Everything has worked out just fine, now let's validate the information.
         * Check to make sure
        */
    } else if (!isUsernameUnique($con)) {
        header('Location: /register?err=1');
        exit;
        /*
         * Now check if the email has been used before or not.
        */
    } else if (!isEmailUnique($con)) {
        header('Location: /register?err=2');
        exit;
        /*
         * Now check if the two passwords match.
        */
    } else if (!doPasswordsMatch()) {
        header('Location: /register?err=3');
        exit;
        /*
         * Everything has checked out, lets enter the info into the database.
        */
    } else {
        echo "1";
        if ($stmt = mysqli_prepare($con, "INSERT INTO PENDING_USERS VALUES(NULL, ?, ?, ?, ?, 1)")) { //USERNAME, PASSWD_HASH, BIO, EMAIL
            mysqli_stmt_bind_param($stmt, "ssss", $username, enc($password, $username), $bio, $email);
            mysqli_stmt_execute($stmt);
            //addToIP($con);
            sendMail();
            mysqli_close($con);
            header('Location: /login?success');
        } else {
            echo mysqli_errno($con);
        }
        mysqli_close($con);
    }
}
/*
 * Checks to see if the two passwords supplied are the same.
 * Returns true if both passwords are the same. Returns false if they don't match.
*/
function doPasswordsMatch() {
    return ($_POST[password0] == $_POST[password]);
}
/*
 * Check the database to see if the email address supplied has been used before.
 * Return true if email is unique, return false if email has been used before.
*/
function isEmailUnique($conn) {
    $res = mysqli_prepare($conn, 'SELECT (SELECT COUNT(*) FROM USERS WHERE EMAIL = ?) + (SELECT COUNT(*) FROM PENDING_USERS WHERE EMAIL = ?) FROM DUAL');
    mysqli_stmt_bind_param($res, 'ss', $_POST[email], $_POST[email]);
    mysqli_stmt_bind_result($res, $num);
    mysqli_stmt_execute($res);
    mysqli_stmt_fetch($res);
    //   echo $num;
    //   exit;
    return ($num == 0) == true;
}
/*
 * Check the database to see if the username supplied has been used before.
 * Return true if username is unique, return false if username has been used before.
*/
function isUsernameUnique($conn) {
    $res = mysqli_prepare($conn, 'SELECT (SELECT COUNT(USERNAME) FROM USERS WHERE USERNAME = ?) + (SELECT COUNT(USERNAME) FROM PENDING_USERS WHERE USERNAME = ?) FROM DUAL');
    mysqli_stmt_bind_param($res, 'ss', $_POST['username'], $_POST['username']);
    mysqli_stmt_bind_result($res, $num);
    mysqli_stmt_execute($res);
    mysqli_stmt_fetch($res);
    //echo $num.' '.($num==0) == true ? 'true' : 'false';
    return ($num == 0) == true;
}
/*
 * Hash the password and the username together
*/
function enc($pwd, $user) {
    return hash('sha512', $pwd . hash('md5', $user));
}
function sani($arg) {
    $a = trim($arg);
    return $a;
}
function addToIP($con) {
    mysqli_query($con, "INSERT INTO IP_STATS(IP, ACCOUNTS) VALUES ('" . $_SERVER[REMOTE_ADDR] . "', 1) ON DUPLICATE KEY UPDATE ACCOUNTS = ACCOUNTS + 1");
}
function getNumRegistered($con) {
    $row = mysqli_query($con, "SELECT ACCOUNTS FROM IP_STATS WHERE IP = '$_SERVER[REMOTE_ADDR]'");
    $arr = mysqli_fetch_array($row);
    return $arr[0] <= 10;
}
function sendMail() {
    //password, email, username, bio
    $subject = 'BetaUp Email Activation';
    $message = '<html><body>Hello, ' . trim($_POST[username]) . '! Welcome to the online beta hub that is BetaUp. To activate your account, <a href = "betaup.org/activate?id=' . enc($_POST[username] . $_POST[bio], $_POST[email]) . '">click this link</a>. Welcome again and enjoy your time! After clicking the link above, you will be able to submit a beta for review or start commenting on different betas around the site, Thank you for your interest in BetaUp!<br><br><br>A BetaUp employee will never give out your password! <b>Never send others your password.</b></body></html>';
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers.= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
    $headers.= "From: BetaUp Confirmation (Do Not Reply)<noreply@betaup.org>" . "\r\n";
    $headers.= "Reply-To: BetaUp Confirmation <info@betaup.org>" . "\r\n";
    $headers.= "Return-Path: BetaUp Confirmation <info@betaup.org>" . "\r\n";
    $headers.= "Organization: BetaUp, The Online Beta Hub\r\n";
    $headers.= "X-Priority: 3\r\n";
    $headers.= "X-Mailer: PHP" . phpversion() . "\r\n";
    //$headers = AddDKIM($headers, $subject, $message).$headers;
    mail($_POST[email], $subject, $message, $headers, "-fnoreply@betaup.org");
    $con = getCon();
    $r = mysqli_query($con, "SELECT MAX(UID) FROM PENDING_USERS");
    $r = mysqli_fetch_array($r);
    mysqli_query($con, "INSERT INTO ACTIVATE VALUES(" . $r[0] . ", '" . enc($_POST[username] . $_POST[bio], $_POST[email]) . "')");
}
?>
