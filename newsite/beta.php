<?php
include "session_info.php";
define('server_val', true);
include_once "mysql_lib.php";
if (empty($_GET['id']) or !is_numeric($_GET['id'])) {
    include "header.php";
    echo "A valid beta id was not supplied. <a href = random>Here</a> is a random beta";
    include "footer.pnp";
    exit;
}
$con = mysqli_connect("localhost", "web", "qwerty23658", "BETAS");
$id = $_GET['id'];
$q = mysqli_query(getCon(), "SELECT SUM(vote) FROM VOTES WHERE BID = " . $_GET['id']);
$score = mysqli_fetch_array($q);
$query = mysqli_query($con, "SELECT TITLE, DESCRIPTION, UPVOTES,DOWNVOTES, UID, USERNAME FROM BETA NATURAL JOIN USERS WHERE BID=" . $_GET['id']);
$beta = mysqli_fetch_array($query);
$rows = mysqli_num_rows($query) < 10 ? mysqli_num_rows($query) : 10;
$row = mysqli_fetch_array($query);
$s = $beta['UPVOTES'] - $beta['DOWNVOTES'];
if (isset($_SESSION['UID'])) {
    $vote = mysqli_query($con, "SELECT IFNULL(vote,0) FROM VOTES WHERE BID =" . $_GET['id'] . " AND UID = " . $_SESSION['UID']);
    $vote = mysqli_fetch_array($vote);
} else $vote = array(0);
mysqli_close($con);
$title = $beta['TITLE'];
include "header.php";
$images = mysqli_query(getCon(), "SELECT link FROM IMAGES WHERE BID = " . $_GET[id]);
$nImages = mysqli_num_rows($images);
if (is_null($nImages)) echo mysqli_error($con);
if (isset($_POST['comment']) && isset($_SESSION['UID'])) {
    $con = getCon();
    //echo $_SESSION[UID].$_POST[comment];
    if (!$stmt = mysqli_prepare($con, "INSERT INTO COMMENTS VALUES(null,?,?,?,NOW())")) echo "Fail"; //Create Prepared Statement
    mysqli_stmt_bind_param($stmt, "iis", $_GET['id'], $_SESSION['UID'], $_POST['comment']); //Bind parameters for comment
    echo "Here";
    mysqli_stmt_execute($stmt);
    //echo "here";
    header("Location: $_SERVER[REQUEST_URI]"); //refresh to show updated content
    exit;
}
?>
    <script>
      function getAjax(){
    var xhr;
    if (window.ActiveXObject)
    {
        try
        {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch(e)
        {
           // alert(e.message);
            xhr = null;
        }
    }
    else
    {
        xhr = new XMLHttpRequest();
    }

    return xhr;
}

function vote(v){
  var a = getAjax();
  a.open("GET","vote?bid=".concat(document.URL.substring(26)).concat("&vote=").concat(v), true);
  a.send();
  return a;
}
function uvote(){
  document.getElementById("upvote").style.color = "#C02";
  document.getElementById("downvote").style.color = "";
  return vote(1);
}

function dvote(){
  document.getElementById("downvote").style.color = "#C02";
  document.getElementById("upvote").style.color = "";
  return vote(-1);
}
    </script>
    <div class="container">
    <div class="jumbotron">
     <?php
echo "<p style='font-size:150%;'><span id=\"upvote\" class=\"glyphicon glyphicon-arrow-up\" style=\"color:" . ($vote[0] == 1 ? "#C02" : "") . "\" onclick=\"uvote();\"></span> " . $s . " <span id=\"downvote\" class=\"glyphicon glyphicon-arrow-down\" style=\"color:" . ($vote[0] == - 1 ? "#C02" : "") . "\"  onclick=\"dvote();\"></span> " . " " . san($beta['TITLE']) . " by <a href = 'user?id=$beta[UID]'>" . san($beta['USERNAME']) . "</a>";
if ($row = mysqli_fetch_row($images)) {
    echo '<center><div style="width=50%;"><img id="img" src="' . $row[0] . '" class="beta-image">';
    echo '</img><br><img src="' . $row[0] . '" id="img1" class="beta-image-thumb" onclick="change(\'' . $row[0] . '\');"></img>';
    while ($row = mysqli_fetch_row($images)) {
        echo '<img src="' . $row[0] . '" id="img1" class="beta-image-thumb" onclick="change(\'' . $row[0] . '\');"></img>';
    }
    echo '</div></center><br><br>';
}
echo "<p>" . san($beta['DESCRIPTION']) . "</p>";
if($_SESSION['rank'] > 2 or $_SESSION['name'] == $beta['USERNAME'])echo "<a style='font-size:50%;' href=delete?id=".$_GET['id'].'>Delete Beta</a>';

echo "<div class = \"container\">";
if (isset($_SESSION['UID'])) echo '<div role="AddComment">
	<form method="post" action="' . $_SERVER['REQUEST_URI'] . '">
		<textarea rows="3" cols="60" placeholder="Type your comment here." name="comment"></textarea><p></p>
                <button type="submit" class="btn" style="top:5000;">Submit Comment</button>
	</form>
	</div>';
echo " <h3><u><b>Recent Comments</b></u></h3>";
$page = (isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 1;
$con = getCon();
if(
$query = mysqli_prepare($con, 'SELECT UID, COMMENT, POSTED, USERNAME FROM COMMENTS NATURAL JOIN USERS WHERE BID= ? ORDER BY POSTED DESC LIMIT '.(($page-1)*10).', 10')){;
mysqli_stmt_bind_param($query, "i", $_GET['id']);
mysqli_stmt_bind_result($query, $UID, $COMMENT, $POSTED, $uname);
mysqli_stmt_execute($query);
while (mysqli_stmt_fetch($query)) {
    mysqli_stmt_fetch($query);
    echo mysqli_error($con);
    echo " <u><h4><a href=\"/user?id=" . $UID . "\">" . san($uname) . "</a> " . $POSTED . "</h4></u><p class=\"lead\">" . san($COMMENT) . "</p>";
}}
echo "</div>";
mysqli_close($con);
?>

    </div>
    </div>
    <script>
    function change(url){
      document.getElementById("img").src = url; 
    }
    </script>
  </BODY>
</HTML>
      <?php include "footer.php"; ?>
