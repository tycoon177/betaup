<?php
define("server_val", true);
include "mysql_lib.php";
if ($_GET['type'] == 'beta') returnBetaInfo();
else if ($_GET['type'] == 'user') returnUserInfo();
else echo "Incorrect api call!";
function returnUserInfo() {
    $con = getCon();
    $stmt = mysqli_prepare($con, "SELECT USERNAME, BIO FROM USERS WHERE UID = ?");
    mysqli_stmt_bind_param($stmt, "i", $_GET['id']);
    mysqli_stmt_bind_result($stmt, $user, $bio);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_fetch($stmt);
    $arr = array("username" => $user, 'BIO' => $bio);
    echo json_encode($arr);
    mysqli_close($con);
}
function returnBetaInfo() {
    $con = getCon();
    $stmt = mysqli_prepare($con, "SELECT USERNAME, NAME TITLE, DESCRIPTION, UPVOTES-DOWNVOTES SCORE, LINK_1,LINK_2, LINK_3,UID FROM BETA NATURAL JOIN USERS WHERE ID = ?");
    mysqli_stmt_bind_param($stmt, "i", $_GET['id']);
    mysqli_stmt_bind_result($stmt, $user, $title, $desc, $score, $l1, $l2, $l3, $uid);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_fetch($stmt);
    $arr = array("username" => $user, "UID" => $uid, "title" => $title, "description" => $desc, "score" => $score, "link_1" => $l1, "link_2" => $l2, "link_3" => $l3);
    echo json_encode($arr);
    mysqli_close($con);
}
