<?
/***************************************************************************\
*  DKIM-CFG ($Id: dkim-cfg-dist.php,v 1.2 2008/09/30 10:21:52 evyncke Exp $)
*  
*  Copyright (c) 2008 
*  Eric Vyncke
*          
* This program is a free software distributed under GNU/GPL licence.
* See also the file GPL.html
*
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
* NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
*THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ***************************************************************************/
 
// Uncomment the $open_SSL_pub and $open_SSL_priv variables and
// copy and paste the content of the public- and private-key files INCLUDING
// the first and last lines (those starting with ----)

$open_SSL_pub="-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC6xK3Q+Hc8WVpWHq3jmASfdBio
zmYwV/H5CuF10kxUkpEdmsEQ1U+KAbRfRGJTyhtPlb2DT94V/yJow7DHPGqbsnq0
orGiBl5aqBXihxELy5vzwHixCxP6j5Txh2UtMjCwP8N1SNgqyFTx631ZIUEfvaW7
E1V1P9dyx8PBURBAlQIDAQAB
-----END PUBLIC KEY-----" ;

$open_SSL_priv="-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQC6xK3Q+Hc8WVpWHq3jmASfdBiozmYwV/H5CuF10kxUkpEdmsEQ
1U+KAbRfRGJTyhtPlb2DT94V/yJow7DHPGqbsnq0orGiBl5aqBXihxELy5vzwHix
CxP6j5Txh2UtMjCwP8N1SNgqyFTx631ZIUEfvaW7E1V1P9dyx8PBURBAlQIDAQAB
AoGADPPQ3hotIrlLo4dTJeKIB4rknuhZe+bNeCkCA8rG0mKrv7LYytMQRhDiMm0q
qIbup/FJiE7Epx/fggZ4Pwj4l589EJ44dDX1lAaEfqwycNK/LzXjCeQuKMjqyeQ8
bYeNu7ptZrEE/8AtgDWLqAnhJFQYHMVVzedt4t4glc9iFGECQQDqbxmeQPuUGIOV
2azFDkSwQmwuMKuZL+FYDeHSli5t5+VsTN/hpQfNvtOvYAEqnL1zdnTDiAYw0NQB
krs1TBDJAkEAy/MOPla3Uh9tSUrYDKLMOM6+4REdw+X1jvgPtHPfnqTeaxWj97+u
maLFAy1Oo7h7AOFT3GpJiFzzX+DurcTDbQJAaIjHzJg/VgckTaJ1dpzpJgtIP8R2
D+WYvvHZu1Q6OcnItGUHA4/aDfVEYRQXERA5Lg4o/LNfx06m6liqX5aeeQJAVczc
Gie7GcZ04JenCrHWfrOjo+Xigs6UnuAHI2B4HDK6C4zgHVM7rhDHF04RPllXDgi9
kgX5B3ArRxPG9b3IhQJBAL9sKtfj1Te/D4UODE27TAZYwlzzfRnhN3LNrcQpAkYy
q5dLJp8X5Ux0inpuqqgarS0EsFw9Pt+VOE/Y6DNtJe0=
-----END RSA PRIVATE KEY-----
" ;

// DKIM Configuration

// Domain of the signing entity (i.e. the email domain)
// This field is mandatory
$DKIM_d='betaup.org' ;  

// Default identity 
// Optional (can be left commented out), defaults to no user @$DKIM_d
$DKIM_i='noreply@betaup.org' ; 

// Selector, defines where the public key is stored in the DNS
//    $DKIM_s._domainkey.$DKIM_d
// Mandatory
$DKIM_s='betaupdkim' ;

?>
