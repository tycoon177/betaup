<?php
include "session_info.php";
if (!isset($_SESSION['rank']) || $_SESSION['rank'] < 2) {
    header("Location: /");
    exit;
}
$con = getCon();
mysqli_query($con, "DELETE FROM PENDING_BETAS WHERE BID = " . $_GET['id']);
mysqli_query($con, "DELETE FROM PENDING_IMAGES WHERE BID = " . $_GET['id']);
header("Location: /projects?pending");
?>
<?php include "footer.php"; ?>
