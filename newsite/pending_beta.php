<?php
include "session_info.php";

define('server_val', true);
include_once "mysql_lib.php";
if(!isset($_SESSION['rank']) or $_SESSION['rank'] < 2){
  header("Location: /projects");
  exit;
}
if (empty($_GET['id']) or !is_numeric($_GET['id'])) {
    include "header.php";
    echo "A valid beta id was not supplied. <a href = random>Here</a> is a random beta";
    include "footer.pnp";
    exit;
}
$con = mysqli_connect("localhost", "web", "qwerty23658", "BETAS");
$id = $_GET['id'];

$query = mysqli_query($con, "SELECT NAME, DESCRIPTION, UID, USERNAME FROM PENDING_BETAS NATURAL JOIN USERS WHERE ID=" . $_GET['id']);
$beta = mysqli_fetch_array($query);
$num =mysqli_num_rows($query);
if($num == 0) {
    include "header.php";
    echo "That beta id does not exist. <a href = random>Here</a> is a random beta";
    include "footer.pnp";
    exit;
}
$row = mysqli_fetch_array($query);

mysqli_close($con);
$title = $beta['NAME'];
include "header.php";
$images = mysqli_query(getCon(), "SELECT link FROM PENDING_IMAGES WHERE BID = " . $_GET[id]);
$nImages = mysqli_num_rows($images);
if (is_null($nImages)) echo mysqli_error($con);
echo "<div class='container'><div class='jumbotron'>";
echo "<p style='font-size:150%;'>".san($beta[NAME]) . " by <a href = 'user?id=$beta[UID]'>" . san($beta[USERNAME]) . "</a>";
if ($row = mysqli_fetch_row($images)) {
    echo '<center><div style="width=50%;"><img id="img" src="' . $row[0] . '" class="beta-image">';
    echo '</img><br><img src="' . $row[0] . '" id="img1" class="beta-image-thumb" onclick="change(\'' . $row[0] . '\');"></img>';
    while ($row = mysqli_fetch_row($images)) {
        echo '<img src="' . $row[0] . '" id="img1" class="beta-image-thumb" onclick="change(\'' . $row[0] . '\');"></img>';
    }
    echo '</div></center><br><br>';
}
echo "<p>" . san($beta['DESCRIPTION']) . "</p>";
echo "<div class = \"container\">";
echo "</div>";
mysqli_close($con);
?>

    </div>
    </div>
    <script>
    function change(url){
      document.getElementById("img").src = url; 
    }
    </script>
  </BODY>
</HTML>
      <?php include "footer.php"; ?>
