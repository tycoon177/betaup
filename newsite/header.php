<HTML lang="en" style="height:100%;">
   <HEAD>
      <TITLE><?php if (isset($title)) echo $title;
      else echo 'The Online Beta Community, BetaUp.org!'; ?></Title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link href="/css/bootstrap.css" rel="stylesheet">
      <link href="/css/bootstrap-responsive.css" rel="stylesheet">
      <link href="/favicon.png" rel="shortcut icon">
      <meta name="keywords" content="Betas,Beta,testing,games,game testing,Betaup,Betaup.org,Online Beta Communtity">
      <meta name="description" content="<?php  if (isset($title)) echo $title;
      else echo 'The Online Beta Community, BetaUp.org!'; ?>"/>
      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-53871987-1', 'auto');
	ga('require', 'displayfeatures');
        ga('send', 'pageview');
      </script>

   </HEAD>
   <BODY style="padding:0;margin:0;height:100%;overflow:auto;">
  <script src="https://code.jquery.com/jquery.js"></script>
  <script src="/js/bootstrap.js"></script>
  <meta name="google-site-verification" content="Kmuj32HN6JE7KEWY-DWywDwTOot1-aukeWkeYzbEhbY" />
    <div style="height:100%;width:90%">
    <div class="left-border"></div>
    <div id="all" class="body-wrapper center container" style="height:100%;bottom:12px;float:center;">
    <div class="container"  role="navigation" style="width:97%;top:5px;float:center;padding:0;">
      <div style="padding-top:-10;"><img src="/BetaUpLogo.png" style="height:50px;float:left;"></img><h2 style="position:relative;left:-3;bottom:-15;">etaUp</h2></div>
      <div class="navbar navbar-inverse navbar-responsive-collapse" style="width:100%;float:left;">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Home</a>
        </div>
        <div class="navbar-collapse collapse" style="padding-bottom:0;">
          <ul class="nav navbar-nav">
            <li><a href="about">About Us</a></li>
            <li class="dropdown">
              <a  class="dropdown-toggle" data-toggle="dropdown">Betas  <b class="caret"></b></a>
              <ul class="dropdown-menu" style="color:#FFF;">
                <li><a id="dlink" href="projects?type=1">Games</a></li>
                <li><a id="dlink" href="projects?type=0">Programs</a></li>
                <li><a id="dlink" href="random">Random Beta</a></li>
                <?php
if (isset($_SESSION['UID'])) {
    echo "<li><a id=\"dlink\" href=submit>Submit Beta</a></li>";
}
?>
              </ul>
            </li>
            <?php if (isset($_SESSION['UID'])) {
    echo "<li><a href=user?id=$_SESSION[UID]>" . san($_SESSION['NAME']) . "</a></li>";
    echo "<li><a href=logout>Logout</a></li>";
} else echo "<li><a href=register>Register</a></li>";
if (isset($_SESSION['rank']) && $_SESSION['rank'] > 1) {
    $q = mysqli_query(getCon(), "SELECT COUNT(*) FROM PENDING_BETAS");
    $row = mysqli_fetch_array($q);
    echo "<li><a href='/projects?pending'>Pending Betas ($row[0])</a></li>";
}
?>
          </ul>
	  <?php
if (!isset($_SESSION['UID'])) echo '
	    <form class="navbar-form navbar-right" role="form" method="post" action="do_login.php" style="padding:0;">
            <div class="form-group" >
              <input type="text" placeholder="Username" class="form-control" name="username">
            </div>
            <div class="form-group" >
              <input type="password" placeholder="Password" class="form-control" name = "password">
            </div>
            <button type="submit" class="btn">Sign in</button>
          </form>
         ';
?>
        </div><!--/.navbar-collapse -->
      </div>
     </div><div style="clear:both;padding:0;"></div><div class="container" style="padding:0;">	
