
	 <div class="jumbotron">
	 <h1>Try Out A Beta Or Upload Your Own!</h1>
	 <p>Join BetaUp and try out some user-uploaded beta programs! Give them feedback, comment, and rate their beta, or upload your own and see what the community thinks!</p>
	 <p><a class="btn btn-primary btn-lg" role="button" href="register">Sign Up Now &raquo;</a></p>
	 </div>
     </div>
	 