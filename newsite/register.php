    <?php
include "session_info.php";
$title = "Register";
include "header.php"; ?>
    <br>
    <div class = "container">
   <?php
/*
	Error codes for registration. 
*/
if (isset($_GET['err'])) {
    $err = $_GET['err'];
    switch ($err) {
        case 0:
            echo "<p style=\"color:red;\">One or more required fields is empty.</p>";
        break;
        case 1:
            echo "<p style=\"color:red;\">This username is not available.</p>";
        break;
        case 2:
            echo "<p style=\"color:red;\">This email is already registered.</p>";
        break;
        case 3:
            echo "<p style=\"color:red;\">The passwords do not match.</p>";
        break;
        case 5:
            echo "<p style=\"color:red;\">You have created too many accounts. If you believe this to be a mistake, contact <a href='mailto:support@betaup.org'>support@betaup.org</a> and we will get back to you as soon as possible.</p>";
    }
}
?>
     <h1><b>Sign Up</b></h1>
     * Required<br><br>
     <form method="post" action="confirmsignup.php" style="padding-bottom:0;">
      <label for="username">Username*</label><br>
      <input type="text" name="username" placeholder="Username" size="75"><br><br>

      <label for="password0">Password*</label><br>
      <input type="password" name="password0" placeholder="Password" size="75"><br><br>

      <label for="password">Confirm Password*</label><br>
      <input type="password" name="password" placeholder="Password" size="75"><br><br>

      <label for="email">Email Address*</label> <br>
      <input type="text" name="email" placeholder="Make sure this is accurate! This will be used to activate your account." size="75"><br><br>

      <label for="interests">Interests</label><br>
      <textarea name="interests" rows="4" cols="78" placeholder="Note: This is optional. Enter in some of your interests. Tell others about yourself!"></textarea><br><br>
      <input type="submit" value="Submit"><br>By clicking 'Submit', you agree to our <a href="/terms">Terms of Service</a>
     </form>     
    </div> </div>
<?php include "footer.php"; ?>
