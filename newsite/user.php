<?php
include "session_info.php";
$id = $_GET["id"];
if (!is_numeric($id)) die("'$id' is not a valid user id.");
$con = mysqli_connect('localhost', 'web', 'qwerty23658', 'BETAS');
$query = mysqli_query($con, 'SELECT USERNAME,BIO FROM USERS WHERE UID = ' . $id);
if (mysqli_num_rows($query) == 0){
  include "header.php";
  die("User $id does not exist at this time");
}
$result = mysqli_fetch_array($query);
$title = $result['USERNAME'];
include "header.php";
?>    
<br><br><br>
    <div class="container">
     <?php
// USER BIO
echo "<div class=\"jumbotron\">";
echo " <h1>" . san($result['USERNAME']) . "</h1>";
echo " <h3><u>About me</u></h3>";
echo "  <p class=\"lead\">" . san($result['BIO']) . "</p>";
mysqli_free_result($result);
//USER BETAS
$query = mysqli_query($con, 'SELECT * FROM BETAS WHERE UID=$id LIMIT 3');
if (mysqli_num_rows($query) <> 0) {
    echo " <h4><u>Most Recently Uploaded Betas</u></h4>";
    while ($result = mysqli_fetch_array($query)) {
        echo "  <p class = \"lead\"><span class=\"glyphicon glyphicon-arrow-up\"></span> " . $result['UPVOTES'] . " <span class=\"glyphicon glyphicon-arrow-down\"></span> " . $result['DOWNVOTES'] . " <a href=\"beta?id=" . $result['ID'] . "\">" . san($result['NAME']) . "</a> " . $result['POSTED'] . "</p>";
    }
    mysqli_free_result($result);
}
//USER RECENT COMMENTS
$query = mysqli_query($con, 'SELECT COMMENT,POSTED,BETA_ID FROM COMMENTS WHERE UID = ' . $id . ' ORDER BY POSTED DESC LIMIT 3');
if (mysqli_num_rows($query) > 0) {
    echo " <h4><u>Recent Comments</u></h4>";
    $query = mysqli_query($con, 'SELECT COMMENT,POSTED,BETA_ID FROM COMMENTS WHERE UID = ' . $id . ' ORDER BY POSTED DESC LIMIT 3');
    while ($result = mysqli_fetch_array($query)) {
        echo " <p class=\"lead\"><a href=\"/beta?id=" . $result['BETA_ID'] . "\">" . $result['POSTED'] . "</a> " . san($result['COMMENT']) . "</p>";
    }
}
echo "</div>";
mysqli_close($con);
?>
    </div>
  </BODY>
</HTML>
<?php include "footer.php"; ?>
