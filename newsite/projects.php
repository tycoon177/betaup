<?php
include "session_info.php";
$table = "BETAS";
$pending = isset($_GET['pending']);
if($pending AND $_SESSION['rank'] > 1)$table = 'PENDING_BETAS';
if($pending AND $_SESSION['rank'] == 1) header("Location: /projects");
$title = "Beta List Page " . ((isset($_GET['page']) && $_GET['page'] > 0) ? $_GET['page'] : 1);
include "header.php"; 
?>    
    <div class="container" style="padding-top:-5;">
      <div class = "jumbotron">
       <ul>
        <?php
$type = isset($_GET['type']) ? $_GET['type'] : 3;
if (!is_numeric($type) || abs($type) > 3) {
    $type = 3;
} else $type = abs($type);
$page = isset($_GET['page']) ? $_GET['page'] : 1;
//if(!is_numeric($page)) $page = 1;
$con = mysqli_connect("localhost", "web", "qwerty23658", "BETAS");
if (mysqli_connect_errno()) {
    echo "Failed to connect to the remote database :( Please let us know so we can fix this!";
    exit;
}
$page = (isset($_GET['page']) && is_numeric($_GET['page']))? $_GET['page'] : 1;
if($pending){
  if ($type <> 3) $query = mysqli_query($con, "SELECT TITLE, DESCRIPTION,BID FROM PENDING_BETAS WHERE IS_GAME=$type LIMIT ".(($page-1)*10).", 10");
  else $query = mysqli_query($con, "SELECT TITLE, DESCRIPTION,BID FROM PENDING_BETAS LIMIT ".(($page-1)*10).", 10");
}else{
  if ($type <> 3) $query = mysqli_query($con, "SELECT TITLE, DESCRIPTION,BID, (UPVOTES-DOWNVOTES) SCORE FROM BETA WHERE IS_GAME=$type ORDER BY UNIX_TIMESTAMP(POSTED)*SCORE DESC"." LIMIT ".(($page-1)*10).", 10");
  else $query = mysqli_query($con, "SELECT TITLE, DESCRIPTION,BID, (UPVOTES-DOWNVOTES) SCORE FROM BETA ORDER BY UNIX_TIMESTAMP(POSTED)*SCORE DESC LIMIT ".(($page-1)*10).", 10");
}
$numrows = mysqli_num_rows($query);
if ($numrows == 0) echo "There doesn't seem to be anything here at the moment. Please check back later.";
while($row = mysqli_fetch_array($query)) {

    if (strlen(trim($row['TITLE'])) == 0 || $i >= 10) {
        break;
    }
    echo '<li><h3>';
    if($pending == false)
      echo "<a href=beta?id=" . intval($row['BID']) . ">";
    echo san($row['TITLE']) . "</a>:</h3>";
    echo "<h5>" . san(substr($row['DESCRIPTION'], 0, 250));
    if (strlen($row['DESCRIPTION'] > 250)) echo '...';
    if($pending == false)
    echo "</h5><h6>Score: " . ($row['SCORE']) . "</h6></li>";
    if($pending){
      echo "<br><a href='approve?id=".$row["BID"]."'>approve</a> <a href = 'reject?id=".$row['BID']."'>reject</a>";
    }
}
echo "</ul></div><p>Page ";
for ($i = 1;$i < $numrows / 10 + 1;$i = $i + 1) {
    if (intval($page) == $i) echo "<b>$i</b>";
    else echo "<a href = \"projects?type=" . $type . "&page=" . $i . "\"> " . $i . " </a>"; 
}
mysqli_close($con);
?>
    </div>
<?php include "footer.php"; ?>
