<?php
include "session_info.php";
$title = "Login";
include ("header.php");
if (isset($_SESSION['UID'])) header("Location: /");
?>
<br><br><br><div class = "container"><center>
<?php
if ($_GET['err'] == "log") echo "<p style=\"color:red;\">You must be signed in to do this.</p>";
else if ($_GET['err'] == "act") echo "<p style=\"color:red;\">Activation failed! Let us know if you see this.</p>";
else if (isset($_GET['success'])) {
    echo "<p style=\"color:green;\">Your account was successfully created. Please confirm your email with the email we sent you.</p>";
}
?>
<form method="post" action="do_login.php">
Username: <input type = "text" name = "username"><br><br>
Password: <input type = "password" name = "password"><br><br>
<button type="submit" class="btn btn-primary btn-lg">Login</button><br>
<a href="register">Need an account?</a></form></cener>
</div>
</BODY></HTML>
<?php include "footer.php"; ?>
